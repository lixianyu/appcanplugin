//
//  EUExPhobosFinderBLEManager.m
//  AppCanPlugin
//
//  Created by Li Xianyu on 14-6-18.
//  Copyright (c) 2014年 zywx. All rights reserved.
//

#import "EUExPhobosBLEManager.h"

@implementation EUExPhobosBLEManager

-(id)initWithBrwView:(EBrowserView *)eInBrwView{
    NSLog(@"%s", __func__);
    self = [super initWithBrwView:eInBrwView];
    NSLog(@"self = %@", self);
    if (self) {
    }
    return self;
}

- (void)initCommon {
    _everConnectedUUID = [NSMutableDictionary dictionaryWithCapacity:20];
    _knownPeripheralsDict = [NSMutableDictionary dictionaryWithCapacity:20];
}

#pragma mark - For Add New Phobos.
- (void)initAddNewPhobos:(NSMutableArray *)array {
    printf("%s", __func__);
    if (_adderManager == nil) {
        _adderManager = [[PhobosAdderBLEManager alloc] init];
        _adderManager.euexBase = self;
    }
    if (_bleManager == nil) {
        _bleManager = [[CBCentralManager alloc] initWithDelegate:_adderManager queue:nil];
    } else {
        _bleManager.delegate = _adderManager;
    }
    _bleState = BLEStateAdd;
}

- (void)scanNewPhobos:(NSMutableArray *)array {
    NSLog(@"%s", __func__);
    [self scanPeripherals];
}

- (void)scanPeripherals {
    NSLog(@"%s", __func__);
    NSDictionary *optionsdict = @{CBCentralManagerScanOptionAllowDuplicatesKey : [NSNumber numberWithBool:YES]};
    [_bleManager scanForPeripheralsWithServices:nil options:optionsdict];
    
    //                NSArray *arrayUUID = [NSArray arrayWithObjects:[CBUUID UUIDWithString:@"853C5886-90EA-7499-847B-96A29C28D2C8"],
    //                                      [CBUUID UUIDWithString:@"1802"], nil];
    //                [_bleManager scanForPeripheralsWithServices:arrayUUID options:nil];
    //[_bleManager scanForPeripheralsWithServices:nil options:nil];
}

- (void)pairPhobos:(NSMutableArray *)array {
    NSLog(@"%s", __func__);
    NSString *uuid = [[array objectAtIndex:0] stringValue];
    [_adderManager pairPhobos:uuid central:_bleManager];
}

#pragma mark - For Find Phobos (Connect already paired Phobos)
- (void)initFindPhobos:(NSMutableArray *)array {
    NSLog(@"%s", __func__);
    [self initCommon];
    if (_finderManager == nil) {
        _finderManager = [[PhobosFinderBLEManager alloc] init];
        _finderManager.euexPhobosBLEManager = self;
    }
    if (_bleManager == nil) {
        _bleManager = [[CBCentralManager alloc] initWithDelegate:_finderManager queue:nil];
    } else {
        _bleManager.delegate = _finderManager;
    }
    _finderManager.cbManager = _bleManager;
    [_finderManager initBLEManager:_bleManager];
    _bleState = BLEStateFind;
}

- (void)findPhobos:(NSMutableArray*)array {
    NSLog(@"%s", __func__);
    //NSString *uuid = [[array objectAtIndex:0] stringValue];
    NSString *uuid = @"853C5886-90EA-7499-847B-96A29C28D2C8";
    NSLog(@"uuid = %@", uuid);
    NSLog(@"_finderManager = %@", _finderManager);
    [_finderManager connectPhobos:uuid];
}


- (void)connectPhobos:(NSMutableArray *)array {
    NSString *uuid = [[array objectAtIndex:0] stringValue];
    [_finderManager connectPhobos:uuid];
}

#pragma mark - Callback for JS.
- (void)callBackHere:(NSString*)command {
    //    [self.meBrwView stringByEvaluatingJavaScriptFromString:command];
    [self performSelector:@selector(callBackDo:) withObject:command afterDelay:.1f];
}

- (void)callBackDo:(NSString*)command {
    [self.meBrwView stringByEvaluatingJavaScriptFromString:command];
}
@end
