//
//  PhobosAdderBLEManager.h
//  AppCanPlugin
//
//  Created by Li Xianyu on 14-6-18.
//  Copyright (c) 2014年 zywx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
//#import "EUExPhobosBLEManager.h"

@class EUExBase;
@interface PhobosAdderBLEManager : NSObject

@property (strong, nonatomic) EUExBase *euexBase;
@end
