//
//  PhobosAdderBLEManager.m
//  AppCanPlugin
//
//  Created by Li Xianyu on 14-6-18.
//  Copyright (c) 2014年 zywx. All rights reserved.
//

#import "PhobosAdderBLEManager.h"

@interface PhobosAdderBLEManager() <CBCentralManagerDelegate, CBPeripheralDelegate>
@property (strong, nonatomic) NSMutableDictionary *foundPeripheral;
@property (strong, nonatomic) NSMutableDictionary *unknowBLEs;
@property (strong, nonatomic) NSMutableArray *uuids;
@property (copy, nonatomic) NSString *curSelectUUID;
@end

@implementation PhobosAdderBLEManager
- (void)pairPhobos:(NSString*)uuid central:(CBCentralManager*)central{
    NSLog(@"%s", __func__);
    [central connectPeripheral:_foundPeripheral[uuid] options:nil];
}

- (BOOL)isPhobos:(NSDictionary *)advertisementData {
    return NO;
}

- (NSInteger)getNewPhobosCounter {
    NSLog(@"%s", __func__);
    NSNumber *oldNumber = [[NSUserDefaults standardUserDefaults] objectForKey:@"newPhobosCounter"];
    if (oldNumber == nil) {
        [self setNewPhobosCounter:1];
        return 0;
    }
    NSInteger counter = [oldNumber integerValue];
    [self setNewPhobosCounter:counter+1];
    return counter;
}

- (void)setNewPhobosCounter:(NSInteger)number {
    NSLog(@"%s: number = %d", __func__, number);
    [[NSUserDefaults standardUserDefaults] setInteger:number forKey:@"newPhobosCounter"];
}

- (NSString *)dataFilePath {
    NSLog(@"%s", __func__);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:@"dataPhobosEver.plist"];
}

#if 1
// 当配对成功后需要调用这个函数保存一下
- (void)savePeripheral:(CBPeripheral*)peripheral {
    NSLog(@"%s", __func__);
    NSString *filePath = [self dataFilePath];
    NSArray *array = nil;
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSLog(@"OH, dataPhobosEver.plist file exist!");
        array = [[NSArray alloc] initWithContentsOfFile:filePath];
    }
    else {
        NSLog(@"OH, dataPhobosEver.plist file does not exist!");
    }
    NSMutableArray *uuidString = [NSMutableArray arrayWithArray:array];
    [uuidString addObject:peripheral.identifier.UUIDString];
    [uuidString writeToFile:filePath atomically:YES];
}
#endif

#pragma mark - CBCentralManagerDelegate
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    NSLog(@"%s", __func__);
    NSLog(@"state = %d", central.state);
    switch (central.state) {
        case CBCentralManagerStatePoweredOn: {
            NSLog(@"CBCentralManagerStatePoweredOn");
            break;
        }
        case CBCentralManagerStatePoweredOff:
            NSLog(@"CBCentralManagerStatePoweredOff");
            break;
        case CBCentralManagerStateResetting:
            NSLog(@"CBCentralManagerStateResetting");
            break;
        case CBCentralManagerStateUnauthorized:
            NSLog(@"CBCentralManagerStateUnauthorized");
            break;
        case CBCentralManagerStateUnknown:
            NSLog(@"CBCentralManagerStateUnknown");
            break;
        case CBCentralManagerStateUnsupported:
            NSLog(@"CBCentralManagerStateUnsupported");
            break;
        default:
            break;
    }
}

//- (void)centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary *)dict {
//    NSLog(@"%s", __func__);
//}

- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals {
    NSLog(@"%s", __func__);
}

- (void)centralManager:(CBCentralManager *)central didRetrieveConnectedPeripherals:(NSArray *)peripherals {
    NSLog(@"%s", __func__);
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)      advertisementData RSSI:(NSNumber *)RSSI {
    NSLog(@"Did discover peripheral. peripheral: %@, rssi: %@, UUID: %@, advertisementData: %@", peripheral, RSSI, peripheral.identifier.UUIDString, advertisementData);
    
    //    _foundPeripheral = peripheral;
    if (_unknowBLEs == nil) {
        _unknowBLEs = [[NSMutableDictionary alloc] initWithCapacity:2];
    }
    if (_foundPeripheral == nil) {
        _foundPeripheral = [[NSMutableDictionary alloc] initWithCapacity:2];
    }
    if (_uuids == nil) {
        _uuids = [[NSMutableArray alloc] initWithCapacity:2];
    }
    
#if 1
    int iRssi = [RSSI intValue];
    NSLog(@"iRssi = %d", iRssi);
    if (iRssi < -37) {
        return;
    }
    NSString *localName = advertisementData[@"kCBAdvDataLocalName"];
    NSLog(@"localName = %@", localName);
    if (![localName isEqualToString:@"Phobos"]) {
        return;
    }
#endif
    
//    NSArray *keys = [advertisementData allKeys];
//    NSData *dataAmb, *dataObj;
//    for (int i = 0; i < [keys count]; ++i) {
//        id key = [keys objectAtIndex: i];
//        NSString *keyName = (NSString *) key;
//        NSObject *value = [advertisementData objectForKey: key];
//        if ([value isKindOfClass: [NSArray class]]) {
//            printf("   key: %s\n", [keyName cStringUsingEncoding: NSUTF8StringEncoding]);
//            NSArray *values = (NSArray *) value;
//            for (int j = 0; j < [values count]; ++j) {
//                if ([[values objectAtIndex: j] isKindOfClass: [CBUUID class]]) {
//                    CBUUID *uuid = [values objectAtIndex: j];
//                    NSData *data = uuid.data;
//                    if (j == 0) {
//                        dataObj = uuid.data;
//                    } else {
//                        dataAmb = uuid.data;
//                    }
//                    printf("      uuid(%d):", j);
//                    for (int j = 0; j < data.length; ++j)
//                        printf(" %02X", ((UInt8 *) data.bytes)[j]);
//                    printf("\n");
//                } else {
//                    const char *valueString = [[value description] cStringUsingEncoding: NSUTF8StringEncoding];
//                    printf("      value(%d): %s\n", j, valueString);
//                }
//            }
//        } else {
//            const char *valueString = [[value description] cStringUsingEncoding: NSUTF8StringEncoding];
//            printf("   key: %s, value: %s\n", [keyName cStringUsingEncoding: NSUTF8StringEncoding], valueString);
//        }
//    }
    
    if ([_unknowBLEs objectForKey:peripheral.identifier.UUIDString] == nil) {
        /*
        for (NSString *uuid in _beginViewController.uuids) {
            if ([uuid isEqualToString:peripheral.identifier.UUIDString]) {
                NSLog(@"%@ alreay in Phobos array, so just return.", uuid);
                return;
            }
        }
        */
        [central stopScan];
        NSLog(@"哦，发现了一个新的待配对的Phobos！");
        NSString *name = @"Phobos";
        NSInteger number = [self getNewPhobosCounter];
        [_unknowBLEs setObject:[name stringByAppendingFormat:@"%d", number] forKey:peripheral.identifier.UUIDString];
        [_foundPeripheral setObject:peripheral forKey:peripheral.identifier.UUIDString];
        [_uuids addObject:peripheral.identifier.UUIDString];
        peripheral.delegate = self;
#if 1
        [central connectPeripheral:peripheral options:nil];
#endif
        
//        [_euexBase.meBrwView stringByEvaluatingJavaScriptFromString:@"uexPhobosBLEManager.didDiscoverPeripheral();"];
        [_euexBase callBackHere:@"uexPhobosBLEManager.didDiscoverPeripheral();"];
    }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s: peripheral = %@", __func__, peripheral);
    NSArray *sUUID = @[[CBUUID UUIDWithString:@"013d8e3b-1877-4d5c-bc59-aaa7e5082346"]];
    [peripheral discoverServices:sUUID];
    
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

#pragma mark - CBPeripheralDelegate
- (void)peripheralDidUpdateName:(CBPeripheral *)peripheral {//NS_AVAILABLE(NA, 6_0);
    NSLog(@"%s", __func__);
}

- (void)peripheralDidInvalidateServices:(CBPeripheral *)peripheral {//NS_DEPRECATED(NA, NA, 6_0, 7_0);
    NSLog(@"%s", __func__);
}

- (void)peripheral:(CBPeripheral *)peripheral didModifyServices:(NSArray *)invalidatedServices {//NS_AVAILABLE(NA, 7_0);
    NSLog(@"%s", __func__);
}

- (void)peripheralDidUpdateRSSI:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    if (error) {
        return;
    }
    if (peripheral.RSSI == nil) {
        return;
    }
    NSLog(@"RSSI = %@", peripheral.RSSI);
}

// 发现了Service
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    for (CBService *s in peripheral.services) {
        NSLog(@"Service found : %@",s.UUID);
//        if ([[[s.UUID UUIDString] lowercaseString] isEqualToString:@"013d8e3b-1877-4d5c-bc59-aaa7e5082346"]) {
//            NSLog(@"Really found!");
//        }
        NSArray *cUUID = @[[CBUUID UUIDWithString:@"51567b6d-a035-499d-a2ea-1e27b5ae8b37"]];
        [peripheral discoverCharacteristics:cUUID forService:s];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverIncludedServicesForService:(CBService *)service error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

// 发现了Characteristic
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    for ( CBCharacteristic *characteristic in service.characteristics ) {
        NSLog(@"characteristic.properties = 0x%x", characteristic.properties);
        [peripheral readValueForCharacteristic:characteristic];
    }
}

// If something had read.
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
    if (!error) {
        //配对成功
        [self savePeripheral:peripheral];
        [_euexBase callBackHere:@"uexPhobosBLEManager.didFinishPairOK();"];
    }
    else {
        [_euexBase callBackHere:@"uexPhobosBLEManager.didFinishPairFail();"];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error {
    NSLog(@"%s, error = %@", __func__, error);
}
@end
