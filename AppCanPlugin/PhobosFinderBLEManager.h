//
//  PhobosFinderBLEManager.h
//  AppCanPlugin
//
//  Created by Li Xianyu on 14-6-18.
//  Copyright (c) 2014年 zywx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
//#import "EUExPhobosBLEManager.h"

// Defines for the TI CC2540 keyfob peripheral
#define TI_KEYFOB_PROXIMITY_ALERT_UUID                      0x1802
#define TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID             0x2a06
#define TI_KEYFOB_PROXIMITY_ALERT_ON_VAL                    0x01
#define TI_KEYFOB_PROXIMITY_ALERT_OFF_VAL                   0x00
#define TI_KEYFOB_PROXIMITY_ALERT_WRITE_LEN                 1

#define TI_KEYFOB_LINK_LOSS_SERVICE_UUID                    0x1803
#define TI_KEYFOB_LINK_LOSS_PROPERTY_UUID                   0x2a06
#define TI_KEYFOB_LINK_LOSS_WRITE_LEN                       1


#define TI_KEYFOB_PROXIMITY_TX_PWR_SERVICE_UUID             0x1804
#define TI_KEYFOB_PROXIMITY_TX_PWR_NOTIFICATION_UUID        0x2A07
#define TI_KEYFOB_PROXIMITY_TX_PWR_NOTIFICATION_READ_LEN    1

#define TI_KEYFOB_BATT_SERVICE_UUID                         0x180F
#define TI_KEYFOB_LEVEL_SERVICE_UUID                        0x2A19
#define TI_KEYFOB_LEVEL_SERVICE_READ_LEN                    1

//#define TI_KEYFOB_ACCEL_SERVICE_UUID                        0xFFA0
//#define TI_KEYFOB_ACCEL_ENABLER_UUID                        0xFFA1
//#define TI_KEYFOB_ACCEL_RANGE_UUID                          0xFFA2
//#define TI_KEYFOB_ACCEL_READ_LEN                            1
//#define TI_KEYFOB_ACCEL_X_UUID                              0xFFA3
//#define TI_KEYFOB_ACCEL_Y_UUID                              0xFFA4
//#define TI_KEYFOB_ACCEL_Z_UUID                              0xFFA5
//
//#define TI_KEYFOB_KEYS_SERVICE_UUID                         0xFFE0
//#define TI_KEYFOB_KEYS_NOTIFICATION_UUID                    0xFFE1
//#define TI_KEYFOB_KEYS_NOTIFICATION_READ_LEN                1

#if defined(GET_TEMPERATURE_USE_BATTERY_SERVICE)
#define TI_KEYFOB_TEMPERATURE_SERVICE_UUID                  0x180F
#define TI_KEYFOB_TEMPERATURE_CHARACTERISTIC                0x2A1C
#else
#define TI_KEYFOB_TEMPERATURE_SERVICE_UUID                  0xFFB0
#define TI_KEYFOB_TEMPERATURE_CHARACTERISTIC                0xFFB1
#endif
#define TI_KEYFOB_TEMPERATURE_CHARACTERISTIC_READ_LEN       4

//@protocol BIDBLEManagerDelegate
//@optional
//- (void)didConnectPeripheral:(NSString*)uuid;
//- (void)didFailToConnectPeripheral:(NSString*)uuid;
//- (void)didDisconnectPeripheral:(NSString*)uuid;
//- (void)peripheralDidUpdateRSSI:(NSString*)uuid RSSI:(NSNumber*)rssi;
//- (void)didDeleteOne:(NSString*)uuid;
//- (void)didUpdateBattery:(float)batteryLevel UUID:(NSString*)uuid;
//- (void)didUpdateTemperature:(float)temperature;
//- (void)stateUpdate;
////@required
//@end

@class EUExPhobosBLEManager;
@interface PhobosFinderBLEManager : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate>
@property (strong, nonatomic) EUExPhobosBLEManager *euexPhobosBLEManager;
//@property (strong, nonatomic) NSMutableDictionary *everConnectedUUID; //所有曾经已连接过的
//@property (strong, nonatomic) NSArray *knownPeripherals;
//@property (strong, nonatomic) NSMutableDictionary *knownPeripheralsDict;
@property (strong, nonatomic) CBCentralManager *cbManager;
//@property (strong, nonatomic) NSMutableDictionary *allConnectPeripheral; //当前所有已连接的
@property (assign, nonatomic) BOOL readingOrWriting;
@property (nonatomic) float batteryLevel;
@property (nonatomic) float tempLevel;

//+ (PhobosFinderBLEManager *)sharedInstance;
//- (BOOL)isConnected:(NSString*)uuid;
//- (void)initBLEManager:(CBCentralManager*)bleManager;
//- (void)connectPhobos:(NSString *)uuid;
//- (void)connectAllPhobos;
//- (void)saveIt:(CBPeripheral*)peripheral;
//- (void)readRSSI;
//- (void)appEnterBackground;
//- (void)ruClosed:(NSString*)uuid;
//- (void)ruOpened:(NSString*)uuid;
//- (void)playWinSound;
//- (void)addDelegateObserver:(id)observer;
//- (void)removeDelegateObserver:(id)observer;
//- (void)chuAlertAllNecessary;
//- (void)saveZuoBiao:(BOOL)flag UUID:(NSString*)uuid longitude:(double)longitude latitude:(double)latitude;
//- (void)stopScanPeripherals;

//- (void)soundBuzzer:(NSString *)uuid;
//- (void)readBattery:(NSString*)uuid;
//- (void)readTemperature:(NSString*)uuid;
//- (void)setLinkLoss:(NSString *)uuid value:(Byte)buzVal;

- (void)connectPhobos:(NSString *)uuid;
- (void)initBLEManager:(CBCentralManager*)bleManager;
@end
