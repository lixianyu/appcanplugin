//
//  PhobosFinderBLEManager.m
//  AppCanPlugin
//
//  Created by Li Xianyu on 14-6-18.
//  Copyright (c) 2014年 zywx. All rights reserved.
//

//#import "PhobosFinderBLEManager.h"
#import "EUExPhobosBLEManager.h"
#import <AudioToolbox/AudioToolbox.h>

typedef NS_ENUM(NSInteger, CommunicateState){
    CommunicateStateIdle = 0,
    CommunicateStateBuzzer,
    CommunicateStateTemperature,
    CommunicateStateBattery,
    CommunicateStateLinkLoss
};

@interface PhobosFinderBLEManager()
@property (assign, nonatomic) CommunicateState communicateState;
@property (assign, nonatomic) NSInteger retValue;
@property (strong, nonatomic) NSString *buzzerUUID;
@property (strong, nonatomic) CBService *buzzerService;
@property (strong, nonatomic) NSString *linkLossUUID;
@property (strong, nonatomic) CBService *linkLossService;
@property (strong, nonatomic) CBService *batteryService;
@property (strong, nonatomic) CBService *temperatureService;

@property (strong, nonatomic) NSString *batteryUUID;
@property (strong, nonatomic) NSString *temperatureUUID;
//@property (strong, nonatomic) CLLocationManager *locationManager;
//@property (strong, nonatomic) CLLocation *curLocation;

@property (strong, nonatomic) NSMutableArray *delegate;

@property (strong, nonatomic) NSTimer *stateCheckTimer; //每隔5秒检查一下状态

@property (strong, nonatomic) CBPeripheral *willConnectPeripheral;
@property (strong, nonatomic) NSString *curUUID;

@end

@implementation PhobosFinderBLEManager {
    SystemSoundID winSoundID;
    Byte linkLossValue;
}

//
//static CGFloat systemVersion;
//+ (BIDBLEManager *)sharedInstance {
//    static dispatch_once_t onceToken;
//    static BIDBLEManager *sSharedInstance;
//    dispatch_once(&onceToken, ^{
//        systemVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
//        NSLog(@"systemVersion = %f", systemVersion);
//        if (systemVersion < 7.0f && systemVersion >= 6.0f) {
//            sSharedInstance = [[BIDBLEiOS6Delegate alloc] init];
//        } else if (systemVersion >= 7.0f){
//            sSharedInstance = [[BIDBLEiOS7Delegate alloc] init];
//        } else {
//            //不支持iOS6.0以下系统
//            //            sSharedInstance = nil;
//            sSharedInstance = [[BIDBLEiOS6Delegate alloc] init];
//        }
//    });
//    NSLog(@"%s: sSharedInstance = %@", __func__, sSharedInstance);
//    return sSharedInstance;
//}

- (void)initBLEManager:(CBCentralManager*)bleManager {
    NSLog(@"%s, _cbManager = %@", __func__, _cbManager);
//    if (_cbManager == nil) {
//        _cbManager = bleManager;
//        if (systemVersion >= 7.0) {
//            _cbManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:@{CBCentralManagerOptionRestoreIdentifierKey : @"phobosCentralManagerIdentifier"}];
//        }
//        
//        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
//        [center addObserver:self selector:@selector(handleHaveDeleteOneConnectedPeripheral:) name:@"Notification_haveDeleteOneConnectedPeripheral" object:nil];
        [self initEverConnectedUUID];
//        _delegate = [NSMutableArray arrayWithCapacity:2];
//        _allConnectPeripheral = [NSMutableDictionary dictionaryWithCapacity:5];
//        [self performSelector:@selector(testit) withObject:nil afterDelay:2.1];
        //        [self testit];
        //        [self testit1];
//    }
}

- (NSString *)dataFilePath {
    NSLog(@"%s", __func__);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:@"dataPhobosEver.plist"];
}

//- (void)saveEverConnectedUUID:(NSString*)uuid {
//    NSLog(@"%s", __func__);
//    NSMutableArray *uuidString = [NSMutableArray arrayWithCapacity:5];
//    for (NSUUID *aNSUUID in _everConnectedUUID) {
//        [uuidString addObject:aNSUUID.UUIDString];
//    }
//    [uuidString writeToFile:[self dataFilePath] atomically:YES];
//}

//在配对新Phobos时候，才会走到这里
//- (void)saveIt:(CBPeripheral*)peripheral {
//    [_allConnectPeripheral setObject:peripheral forKey:peripheral.identifier.UUIDString];
//    [_everConnectedUUID setObject:peripheral.identifier.UUIDString forKey:peripheral.identifier];
//    NSLog(@"_everConnectedUUID.count = %d", _everConnectedUUID.count);
//    [self saveEverConnectedUUID:nil];
//    NSLog(@"delegate.count = %d", _delegate.count);
//    for (id targetDelegate in _delegate) {
//        [self updateTheState:targetDelegate];
//    }
//}
#if 0
- (void)initEverConnectedUUID {
    NSLog(@"%s", __func__);
    NSString *filePath = [self dataFilePath];
    _everConnectedUUID = [NSMutableDictionary dictionaryWithCapacity:5];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSArray *array = [[NSArray alloc] initWithContentsOfFile:filePath];
        for (NSString *uuid in array) {
            [_everConnectedUUID setObject:uuid forKey:[[NSUUID alloc] initWithUUIDString:uuid]];
        }
    }
    _knownPeripherals = [_cbManager retrievePeripheralsWithIdentifiers:[_everConnectedUUID allKeys]];
    NSLog(@"%s: _knownPeripherals = %@", __func__, _knownPeripherals);
    _knownPeripheralsDict = [NSMutableDictionary dictionaryWithCapacity:5];
    NSString *uuid = nil;
    for (CBPeripheral *aPeripheral in _knownPeripherals) {
        [_knownPeripheralsDict setObject:aPeripheral forKey:aPeripheral.identifier.UUIDString];
        aPeripheral.delegate = self;
        uuid = aPeripheral.identifier.UUIDString;
        _willConnectPeripheral = aPeripheral;
    }
    NSLog(@"%s: _knownPeripheralsDict = %@", __func__, _knownPeripheralsDict);
    CBPeripheral *aPeripheral = _knownPeripheralsDict[uuid];
    NSLog(@"hahaha0 = %@", aPeripheral);
    NSLog(@"hahaha1 = %@", _knownPeripheralsDict[uuid]);
}
#else
- (void)initEverConnectedUUID {
    NSLog(@"%s", __func__);
    NSString *filePath = [self dataFilePath];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSArray *array = [[NSArray alloc] initWithContentsOfFile:filePath];
        for (NSString *uuid in array) {
            [_euexPhobosBLEManager.everConnectedUUID setObject:uuid forKey:[[NSUUID alloc] initWithUUIDString:uuid]];
        }
    }
    _euexPhobosBLEManager.knownPeripherals = [_cbManager retrievePeripheralsWithIdentifiers:[_euexPhobosBLEManager.everConnectedUUID allKeys]];
    NSLog(@"%s: _knownPeripherals = %@", __func__, _euexPhobosBLEManager.knownPeripherals);
    NSString *uuid = nil;
    for (CBPeripheral *aPeripheral in _euexPhobosBLEManager.knownPeripherals) {
        [_euexPhobosBLEManager.knownPeripheralsDict setObject:aPeripheral forKey:aPeripheral.identifier.UUIDString];
        aPeripheral.delegate = self;
        uuid = aPeripheral.identifier.UUIDString;
        _willConnectPeripheral = aPeripheral;
    }
    NSLog(@"%s: _knownPeripheralsDict = %@", __func__, _euexPhobosBLEManager.knownPeripheralsDict);
    CBPeripheral *aPeripheral = _euexPhobosBLEManager.knownPeripheralsDict[uuid];
    NSLog(@"hahaha0 = %@", aPeripheral);
    NSLog(@"hahaha1 = %@", _euexPhobosBLEManager.knownPeripheralsDict[uuid]);
}
#endif

- (void)testit {
#ifdef DEBUG_BLE_STATE
    kkkkkk
    if (systemVersion >= 7.0) {
        NSArray *knownPeripherals1 = [_cbManager retrievePeripheralsWithIdentifiers:[_everConnectedUUID allKeys]];
        NSLog(@"%s: Already Known Peripherals = %@", __func__, knownPeripherals1);
        [self performSelector:@selector(testit) withObject:nil afterDelay:3.1];
    }
#else
    // Nothing
#endif
}

- (void)testit1 {
    //    UILocalNotification *notification=[[UILocalNotification alloc] init];
    //    if (notification!=nil)
    //    {
    //        NSDate *now=[NSDate new];
    //        //notification.fireDate=[now addTimeInterval:period];
    //        notification.fireDate = [now dateByAddingTimeInterval:10];
    //        NSLog(@"%d",10);
    //        notification.timeZone=[NSTimeZone defaultTimeZone];
    //        notification.soundName = @"Bell.mp3";
    //        //notification.alertBody=@"TIME！";
    //        notification.alertBody = [NSString stringWithFormat:@"@%时间到了!", @"起床"];
    //        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    //    }
}

- (void)doConnectPhobos:(CBPeripheral*)aPeripheral {
    NSLog(@"%s, uuid = %@", __func__, aPeripheral.identifier.UUIDString);
    //    NSDictionary *connectOptions = @{CBConnectPeripheralOptionNotifyOnConnectionKey : [NSNumber numberWithBool:NO],
    //                                     CBConnectPeripheralOptionNotifyOnDisconnectionKey : [NSNumber numberWithBool:NO],
    //                                     CBConnectPeripheralOptionNotifyOnNotificationKey : [NSNumber numberWithBool:YES]};
    [_cbManager connectPeripheral:aPeripheral options:nil];
    //    [_cbManager connectPeripheral:aPeripheral options:connectOptions];
}

- (void)cancelConnectPhobos:(NSString*)uuid {
    NSLog(@"%s", __func__);
//    _knownPeripherals = [_cbManager retrievePeripheralsWithIdentifiers:[_everConnectedUUID allKeys]];
//    NSLog(@"%s: _knownPeripherals = %@", __func__, _knownPeripherals);
//    if (_knownPeripherals.count > 0) {
//        for (CBPeripheral *aPeripheral in _knownPeripherals) {
//            if ([aPeripheral.identifier.UUIDString isEqualToString:uuid]) {
//                [_cbManager cancelPeripheralConnection:aPeripheral];
//                break;
//            }
//        }
//    }
    [_cbManager cancelPeripheralConnection:_euexPhobosBLEManager.knownPeripheralsDict[uuid]];
}

- (BOOL)isInRadarView {
    NSLog(@"%s", __func__);
//    if (isPad) { //iPad的界面，雷达界面是一直显示的
//        NSLog(@"Yes, I'm iPad.");
//        return YES;
//    } else {
//        NSLog(@"Yes, I'm iPhone.");
//        return [BIDRadarViewController6 isInRadarView];
//    }
    return YES;
}

- (BOOL)ifNeedConnect:(NSString*)uuid {
    NSLog(@"%s", __func__);
//    if (![self isInRadarView]) {
//        return NO;
//    }
//    
//    for (id aObserver in _delegate) {
//        if ([aObserver isKindOfClass:[BIDRadarViewController6 class]]) {
//            NSLog(@"Yes, got BIDRadarViewController6");
//            BIDRadarViewController6 *radarViewController = (BIDRadarViewController6*)aObserver;
//            if ([radarViewController isChecked:uuid]) {
//                NSLog(@"isChecked return YES");
//                return YES;
//            } else {
//                NSLog(@"isChecked return NO");
//            }
//            break;
//        }
//        if ([aObserver isKindOfClass:[BIDRadarViewController6iPad class]]) {
//            NSLog(@"Yes, got BIDRadarViewController6iPad");
//            BIDRadarViewController6iPad *radarViewController = (BIDRadarViewController6iPad*)aObserver;
//            if ([radarViewController isChecked:uuid]) {
//                NSLog(@"isChecked return YES");
//                return YES;
//            } else {
//                NSLog(@"isChecked return NO");
//            }
//            break;
//        }
//    }
    return YES;
}

- (void)connectPhobos:(NSString *)uuid {
    NSLog(@"%s, uuid = %@", __func__, uuid);
//    CBPeripheral *peripheral = _knownPeripheralsDict[uuid];
//    NSLog(@"peripheral = %@", peripheral);
//    if (peripheral) {
//        if (peripheral.state == CBPeripheralStateConnected ||
//            peripheral.state == CBPeripheralStateConnecting) {
//            NSLog(@"%@ already connected , so just return.", uuid);
//            return;
//        }
//    }
    if ([self isInRadarView]) {
        NSLog(@"Yes, I'm in radar view.");
//        _curUUID = uuid;
//        [self scanPeripherals];
    } else {
        NSLog(@"Yes, I'm not in radar view, so do not scan.");
    }
    
//    _knownPeripherals = [_cbManager retrievePeripheralsWithIdentifiers:[_everConnectedUUID allKeys]];
//    NSMutableArray *identifier = [[NSMutableArray alloc] initWithCapacity:1];
//    [identifier addObject:[[NSUUID alloc] initWithUUIDString:uuid]];
//    _knownPeripherals = [_cbManager retrievePeripheralsWithIdentifiers:identifier];
//    NSLog(@"%s: _knownPeripherals = %@", __func__, _knownPeripherals);
//    if (_knownPeripherals.count > 0) {
//        for (CBPeripheral *aPeripheral in _knownPeripherals) {
//            if ([aPeripheral.identifier.UUIDString isEqualToString:uuid]) {
//                [self doConnectPhobos:aPeripheral];
//                break;
//            }
//        }
//    }
//    _everConnectedUUID = nil;
//    [self initEverConnectedUUID];
    
    NSLog(@"_cbManager = %@", _cbManager);
//    NSLog(@"_everConnectedUUID = %@", _everConnectedUUID);
//    _knownPeripherals = [_cbManager retrievePeripheralsWithIdentifiers:[_everConnectedUUID allKeys]];
//    NSLog(@"%s: _knownPeripherals = %@", __func__, _knownPeripherals);
//    _knownPeripheralsDict = [NSMutableDictionary dictionaryWithCapacity:5];
//    for (CBPeripheral *aPeripheral in _knownPeripherals) {
//        [_knownPeripheralsDict setObject:aPeripheral forKey:aPeripheral.identifier.UUIDString];
//        aPeripheral.delegate = self;
//        _willConnectPeripheral = aPeripheral;
//    }
    
    NSLog(@"_knownPeripheralsDict = %@", _euexPhobosBLEManager.knownPeripheralsDict);
#if 1
    [self doConnectPhobos:_euexPhobosBLEManager.knownPeripheralsDict[uuid]];
#else
    [self doConnectPhobos:_willConnectPeripheral];
#endif
}

- (void)scanPeripherals {
    NSLog(@"%s", __func__);
    NSDictionary *optionsdict = @{CBCentralManagerScanOptionAllowDuplicatesKey : [NSNumber numberWithBool:YES]};
    [_cbManager scanForPeripheralsWithServices:nil options:optionsdict];
    
    //                NSArray *arrayUUID = [NSArray arrayWithObjects:[CBUUID UUIDWithString:@"853C5886-90EA-7499-847B-96A29C28D2C8"],
    //                                      [CBUUID UUIDWithString:@"1802"], nil];
    //                [_bleManager scanForPeripheralsWithServices:arrayUUID options:nil];
//    [_cbManager scanForPeripheralsWithServices:nil options:nil];
}

- (void)stopScanPeripherals {
    NSLog(@"%s", __func__);
    [_cbManager stopScan];
}

//- (void)connectAllPhobos {
//    NSLog(@"%s", __func__);
//    NSLog(@"_knownPeripherals = %@", _knownPeripherals);
//    if (_knownPeripherals.count > 0) {
//        for (CBPeripheral *aPeripheral in _knownPeripherals) {
//            [self doConnectPhobos:aPeripheral];
//        }
//    }
//}

- (BOOL)isConnected:(NSString*)uuid {
//    NSLog(@"%s,uuid=%@, _allConnectPeripheral.count = %d", __func__, uuid, _knownPeripheralsDict.count);
    CBPeripheral *peripheral = _euexPhobosBLEManager.knownPeripheralsDict[uuid];
    if (peripheral.state == CBPeripheralStateConnected) {
        return YES;
    }
    return NO;
}

- (void)readRSSI {
    NSLog(@"%s", __func__);
    for (NSString *uuid in _euexPhobosBLEManager.knownPeripheralsDict) {
        [_euexPhobosBLEManager.knownPeripheralsDict[uuid] readRSSI];
    }
}

//- (void)updateTheState:(id)targetDelegate {
//    if ([targetDelegate respondsToSelector:@selector(stateUpdate)]) {
//        [targetDelegate stateUpdate];
//    }
//}

/** 只有未连接的Phobos才会进入到这里
 */
- (void)ruOpened:(NSString*)uuid {
    NSLog(@"%s", __func__);
    [self connectPhobos:uuid];
}

- (void)ruClosed:(NSString*)uuid {
    NSLog(@"%s", __func__);
    [self cancelConnectPhobos:uuid];
}


/**
 @param chuOrRu YES means chu alert, and NO means ru alert.
 @param time 延迟多少秒后触发
 */
- (void)setLocalNotification:(NSMutableDictionary*)info chuOrRu:(BOOL)chuOrRu TimeInterval:(NSTimeInterval)time {
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    if (notification != nil) {
        NSDate *now = [NSDate new];
        notification.fireDate = [now dateByAddingTimeInterval:0.5];
        notification.timeZone=[NSTimeZone defaultTimeZone];
        notification.soundName = @"Bell1.wav";
        if (chuOrRu) {
            notification.alertBody = [NSString stringWithFormat:@"出围报警：%@", info[@"name"]];
            [info setObject:[NSNumber numberWithBool:NO] forKey:@"ru"];
        } else {
            notification.alertBody = [NSString stringWithFormat:@"入围报警：%@", info[@"name"]];
            [info setObject:[NSNumber numberWithBool:NO] forKey:@"chu"];
        }
        notification.userInfo = info;
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    }
}

/** 播放报警音乐
 */
- (void)playWinSound {
    NSLog(@"%s", __func__);
    if (winSoundID == 0) {
        NSURL *soundURL = [[NSBundle mainBundle] URLForResource:@"Bell1"
                                                  withExtension:@"wav"];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundURL,
                                         &winSoundID);
    }
    AudioServicesPlaySystemSound(winSoundID);
}

#pragma mark - CBCentralManagerDelegate
- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    NSLog(@"%s", __func__);
    NSLog(@"state = %d", central.state);
    switch (central.state) {
        case CBCentralManagerStatePoweredOn: {
            NSLog(@"CBCentralManagerStatePoweredOn");
            break;
        }
        case CBCentralManagerStatePoweredOff:
            NSLog(@"CBCentralManagerStatePoweredOff");
            break;
        case CBCentralManagerStateResetting:
            NSLog(@"CBCentralManagerStateResetting");
            break;
        case CBCentralManagerStateUnauthorized:
            NSLog(@"CBCentralManagerStateUnauthorized");
            break;
        case CBCentralManagerStateUnknown:
            NSLog(@"CBCentralManagerStateUnknown");
            break;
        case CBCentralManagerStateUnsupported:
            NSLog(@"CBCentralManagerStateUnsupported");
            break;
        default:
            break;
    }
}

//- (void)centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary *)dict {
//    NSLog(@"%s: self = %@", __func__, self);
//    NSLog(@"central is : %@", central);
//    id idInstance = [BIDBLEManager sharedInstance];
//    NSLog(@"idInstance = %@", idInstance);
//    NSArray *peripherals = dict[CBCentralManagerRestoredStatePeripheralsKey];
//    NSLog(@"restored peripherals = %@", peripherals);
//    BIDAppDelegate *appDelegate = (BIDAppDelegate*)[UIApplication sharedApplication].delegate;
//    [appDelegate startBackgroundTask];// 得到7秒的后台运行机会
//}

- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals {
    NSLog(@"%s", __func__);
}

- (void)centralManager:(CBCentralManager *)central didRetrieveConnectedPeripherals:(NSArray *)peripherals {
    NSLog(@"%s", __func__);
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *) advertisementData RSSI:(NSNumber *)RSSI {
    NSLog(@"%s: uuid = %@, _curUUID = %@", __func__, peripheral.identifier.UUIDString, _curUUID);
    if (![peripheral.identifier.UUIDString isEqualToString:_curUUID]) {
        return;
    }
    [self stopScanPeripherals];
    NSLog(@"peripheral = %@", peripheral);
//    _willConnectPeripheral = peripheral;
    _euexPhobosBLEManager.p = peripheral;
    peripheral.delegate = self;
    [self doConnectPhobos:peripheral];
    NSLog(@"_willConnectPeripheral = %@", _willConnectPeripheral);
//    NSArray *knownPeripherals = [_cbManager retrievePeripheralsWithIdentifiers:[_everConnectedUUID allKeys]];
//    NSLog(@"%s: knownPeripherals = %@", __func__, knownPeripherals);
//    if (knownPeripherals.count > 0) {
//        for (CBPeripheral *aPeripheral in knownPeripherals) {
//            if ([aPeripheral.identifier.UUIDString isEqualToString:peripheral.identifier.UUIDString]) {
//                if ([self ifNeedConnect:peripheral.identifier.UUIDString]) {
//                    NSLog(@"Yes, need connect %@", peripheral.identifier.UUIDString);
//                    [self doConnectPhobos:aPeripheral];
//                    break;
//                }
//            }
//        }
//    }
//    CBPeripheral *thePeripheral = _knownPeripheralsDict[peripheral.identifier.UUIDString];
//    NSLog(@"thePeripheral = %@", thePeripheral);
//    NSLog(@"peripheral = %@", peripheral);
//    if (peripheral) {
//        [self doConnectPhobos:peripheral];
//    }
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s: uuid = %@", __func__, peripheral.identifier.UUIDString);
    peripheral.delegate = self;
//    [_allConnectPeripheral setObject:peripheral forKey:peripheral.identifier.UUIDString];
//    NSLog(@"_allConnectPeripheral.count = %d", _allConnectPeripheral.count);
//    [self ruAlertNow:peripheral.identifier.UUIDString];
//    for (id targetDelegate in _delegate) {
//        if ([targetDelegate respondsToSelector:@selector(didConnectPeripheral:)]) {
//            [targetDelegate didConnectPeripheral:peripheral.identifier.UUIDString];
//        }
//        [self updateTheState:targetDelegate];
//    }
    NSLog(@"applicationState = %d", [UIApplication sharedApplication].applicationState);
    NSLog(@"_euexPhobosBLEManager.p = %@", _euexPhobosBLEManager.p);
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s: error = %@", __func__, error);
//    [_allConnectPeripheral removeObjectForKey:peripheral.identifier.UUIDString];
//    for (id targetDelegate in _delegate) {
//        if ([targetDelegate respondsToSelector:@selector(didFailToConnectPeripheral:)]) {
//            [targetDelegate didFailToConnectPeripheral:peripheral.identifier.UUIDString];
//        }
//        [self updateTheState:targetDelegate];
//    }
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s: error = %@", __func__, error);
//    [_allConnectPeripheral removeObjectForKey:peripheral.identifier.UUIDString];
//    [self chuAlertNow:peripheral.identifier.UUIDString];
//    for (id targetDelegate in _delegate) {
//        if ([targetDelegate respondsToSelector:@selector(didDisconnectPeripheral:)]) {
//            [targetDelegate didDisconnectPeripheral:peripheral.identifier.UUIDString];
//        }
//        [self updateTheState:targetDelegate];
//    }
}

#pragma mark - CBPeripheralDelegate
- (void)peripheralDidUpdateName:(CBPeripheral *)peripheral {//NS_AVAILABLE(NA, 6_0);
    NSLog(@"%s", __func__);
}

- (void)peripheralDidUpdateRSSI:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"%s: error = %@", __func__, error);
    if (error) {
        NSLog(@"error = %@", error);
        return;
    }
    if (peripheral.RSSI == nil) {
        return;
    }
    for (id targetDelegate in _delegate) {
        if ([targetDelegate respondsToSelector:@selector(peripheralDidUpdateRSSI:RSSI:)]) {
            [targetDelegate peripheralDidUpdateRSSI:peripheral.identifier.UUIDString RSSI:peripheral.RSSI];
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    NSLog(@"%s, _communicateState = %d", __func__, _communicateState);
    CBUUID *aCBUUID;
    if (_communicateState == CommunicateStateBuzzer) {
        aCBUUID = [CBUUID UUIDWithString:@"1802"];
    } else if (_communicateState == CommunicateStateLinkLoss) {
        aCBUUID = [CBUUID UUIDWithString:@"1803"];
    } else if (_communicateState == CommunicateStateBattery) {
        aCBUUID = [CBUUID UUIDWithString:@"180f"];
    } else if (_communicateState == CommunicateStateTemperature) {
        aCBUUID = [CBUUID UUIDWithString:@"ffb0"];
    }
    for (CBService *service in peripheral.services) {
        NSLog(@"service = %@", service);
        if (1 == [self compareCBUUID:service.UUID UUID2:aCBUUID]) {
            NSLog(@"Yes, got it.");
            if (_communicateState == CommunicateStateBuzzer) {
                NSLog(@"Yes, got 1802.");
                _buzzerService = service;
                [self performSelector:@selector(soundBuzzer2) withObject:nil afterDelay:0.1];
            } else if (_communicateState == CommunicateStateLinkLoss) {
                NSLog(@"Yes, got 1803.");
                _linkLossService = service;
                [self performSelector:@selector(setLinkLoss2) withObject:nil afterDelay:0.1];
            } else if (_communicateState == CommunicateStateBattery) {
                NSLog(@"Yes, got 180f.");
                _batteryService = service;
                [self performSelector:@selector(readBattery2) withObject:nil afterDelay:0.1];
            } else if (_communicateState == CommunicateStateTemperature) {
                NSLog(@"Yes, got 180f -- temperature.");
                _temperatureService = service;
                [self performSelector:@selector(readTemperature2) withObject:nil afterDelay:0.1];
            }
            return;
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverIncludedServicesForService:(CBService *)service error:(NSError *)error {
    NSLog(@"%s", __func__);
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    NSLog(@"%s: service = %@, error = %@, _communicateState = %d", __func__, service, error, _communicateState);
    if (_communicateState == CommunicateStateBuzzer) {
        [self performSelector:@selector(soundBuzzer3) withObject:nil afterDelay:0.1];
    } else if (_communicateState == CommunicateStateLinkLoss) {
        [self performSelector:@selector(setLinkLoss3) withObject:nil afterDelay:0.1];
    } else if (_communicateState == CommunicateStateBattery) {
        [self performSelector:@selector(readBattery3) withObject:nil afterDelay:0.1];
    } else if (_communicateState == CommunicateStateTemperature) {
        [self performSelector:@selector(readTemperature3) withObject:nil afterDelay:0.1];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s: characteristic = %@, error = %@", __func__, characteristic, error);
    UInt16 characteristicUUID = [self CBUUIDToInt:characteristic.UUID];
    if (!error) {
        switch (characteristicUUID) {
            case TI_KEYFOB_LEVEL_SERVICE_UUID: {
                char batlevel;
                [characteristic.value getBytes:&batlevel length:TI_KEYFOB_LEVEL_SERVICE_READ_LEN];
                _batteryLevel = (float)batlevel;
                NSLog(@"_batteryLevel = %f", _batteryLevel);
                for (id targetDelegate in _delegate) {
                    if ([targetDelegate respondsToSelector:@selector(didUpdateBattery:UUID:)]) {
                        [targetDelegate didUpdateBattery:_batteryLevel UUID:_batteryUUID];
                    }
                }
                break;
            }
                
            case TI_KEYFOB_TEMPERATURE_CHARACTERISTIC: {
                SInt32 tempLevel;
                [characteristic.value getBytes:&tempLevel length:TI_KEYFOB_TEMPERATURE_CHARACTERISTIC_READ_LEN];
                float tempHight, tempLow;
                tempHight = (float)(tempLevel >> 16);
                tempLow = (float)(tempLevel & 0x0000FFFF);
                NSLog(@"tempHight = %f, tempLow = %f", tempHight, tempLow);
                _tempLevel = tempHight + (tempLow/10);
                NSLog(@"temperature = %f", _tempLevel);
                for (id targetDelegate in _delegate) {
                    if ([targetDelegate respondsToSelector:@selector(didUpdateTemperature:)]) {
                        [targetDelegate didUpdateTemperature:_tempLevel];
                    }
                }
                break;
            }
            default:
                break;
        }
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s, peripheral=%@, characteristic=%@, error=%@", __func__, peripheral, characteristic, error);
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s", __func__);
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverDescriptorsForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    NSLog(@"%s", __func__);
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error {
    NSLog(@"%s", __func__);
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForDescriptor:(CBDescriptor *)descriptor error:(NSError *)error {
    NSLog(@"%s", __func__);
}

#pragma mark - Read&Write
/*
 *  @method compareCBUUID
 *
 *  @param UUID1 UUID 1 to compare
 *  @param UUID2 UUID 2 to compare
 *
 *  @returns 1 (equal) 0 (not equal)
 *
 *  @discussion compareCBUUID compares two CBUUID's to each other and returns 1 if they are equal and 0 if they are not
 *
 */

-(int) compareCBUUID:(CBUUID *)UUID1 UUID2:(CBUUID *)UUID2 {
    char b1[16];
    char b2[16];
    [UUID1.data getBytes:b1];
    [UUID2.data getBytes:b2];
    if (memcmp(b1, b2, UUID1.data.length) == 0) return 1;
    else return 0;
}

/*
 *  @method CBUUIDToInt
 *
 *  @param UUID1 UUID 1 to convert
 *
 *  @returns UInt16 representation of the CBUUID
 *
 *  @discussion CBUUIDToInt converts a CBUUID to a Uint16 representation of the UUID
 *
 */
-(UInt16) CBUUIDToInt:(CBUUID *) UUID {
    char b1[16];
    [UUID.data getBytes:b1];
    return ((b1[0] << 8) | b1[1]);
}

/*!
 *  @method swap:
 *
 *  @param s Uint16 value to byteswap
 *
 *  @discussion swap byteswaps a UInt16
 *
 *  @return Byteswapped UInt16
 */

-(UInt16) swap:(UInt16)s {
    UInt16 temp = s << 8;
    temp |= (s >> 8);
    return temp;
}

/*
 *  @method CBUUIDToString
 *
 *  @param UUID UUID to convert to string
 *
 *  @returns Pointer to a character buffer containing UUID in string representation
 *
 *  @discussion CBUUIDToString converts the data of a CBUUID class to a character pointer for easy printout using printf()
 *
 */
-(const char *) CBUUIDToString:(CBUUID *) UUID {
    return [[UUID.data description] cStringUsingEncoding:NSStringEncodingConversionAllowLossy];
}


/*
 *  @method UUIDToString
 *
 *  @param UUID UUID to convert to string
 *
 *  @returns Pointer to a character buffer containing UUID in string representation
 *
 *  @discussion UUIDToString converts the data of a CFUUIDRef class to a character pointer for easy printout using printf()
 *
 */
-(const char *) UUIDToString:(CFUUIDRef)UUID {
    if (!UUID) return "NULL";
    CFStringRef s = CFUUIDCreateString(NULL, UUID);
    return CFStringGetCStringPtr(s, 0);
    
}

/*
 *  @method findServiceFromUUID:
 *
 *  @param UUID CBUUID to find in service list
 *  @param p Peripheral to find service on
 *
 *  @return pointer to CBService if found, nil if not
 *
 *  @discussion findServiceFromUUID searches through the services list of a peripheral to find a
 *  service with a specific UUID
 *
 */
-(CBService *) findServiceFromUUID:(CBUUID *)UUID p:(CBPeripheral *)p {
    for(int i = 0; i < p.services.count; i++) {
        CBService *s = [p.services objectAtIndex:i];
        if ([self compareCBUUID:s.UUID UUID2:UUID]) return s;
    }
    return nil; //Service not found on this peripheral
}

/*
 *  @method findCharacteristicFromUUID:
 *
 *  @param UUID CBUUID to find in Characteristic list of service
 *  @param service Pointer to CBService to search for charateristics on
 *
 *  @return pointer to CBCharacteristic if found, nil if not
 *
 *  @discussion findCharacteristicFromUUID searches through the characteristic list of a given service
 *  to find a characteristic with a specific UUID
 *
 */
-(CBCharacteristic *) findCharacteristicFromUUID:(CBUUID *)UUID service:(CBService*)service {
    for(int i=0; i < service.characteristics.count; i++) {
        CBCharacteristic *c = [service.characteristics objectAtIndex:i];
        if ([self compareCBUUID:c.UUID UUID2:UUID]) return c;
    }
    return nil; //Characteristic not found on this service
}

/*!
 *  @method writeValue:
 *
 *  @param serviceUUID Service UUID to write to (e.g. 0x2400)
 *  @param characteristicUUID Characteristic UUID to write to (e.g. 0x2401)
 *  @param data Data to write to peripheral
 *  @param p CBPeripheral to write to
 *  @param flag If true, then write with CBCharacteristicWriteWithResponse
 *
 *  @discussion Main routine for writeValue request, writes without feedback. It converts integer into
 *  CBUUID's used by CoreBluetooth. It then searches through the peripherals services to find a
 *  suitable service, it then checks that there is a suitable characteristic on this service.
 *  If this is found, value is written. If not nothing is done.
 *
 */

- (NSInteger)writeValue:(int)serviceUUID characteristicUUID:(int)characteristicUUID p:(CBPeripheral *)p data:(NSData *)data Response:(BOOL)flag{
    UInt16 s = [self swap:serviceUUID];
    UInt16 c = [self swap:characteristicUUID];
    NSData *sd = [[NSData alloc] initWithBytes:(char *)&s length:2];
    NSData *cd = [[NSData alloc] initWithBytes:(char *)&c length:2];
    CBUUID *su = [CBUUID UUIDWithData:sd];
    CBUUID *cu = [CBUUID UUIDWithData:cd];
    CBService *service = [self findServiceFromUUID:su p:p];
    if (!service) {
        NSLog(@"Could not find service with UUID %s on peripheral with UUID %@\r\n",(char*)[self CBUUIDToString:su],p.identifier.UUIDString);
        return 1;
    }
    CBCharacteristic *characteristic = [self findCharacteristicFromUUID:cu service:service];
    if (!characteristic) {
        NSLog(@"Could not find characteristic with UUID %s on service with UUID %s on peripheral with UUID %@\r\n", (char*)[self CBUUIDToString:cu],[self CBUUIDToString:su], p.identifier.UUIDString);
        return 2;
    }
    if (flag) {
        [p writeValue:data forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
    } else {
        [p writeValue:data forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
    }
    return 0;
}


/*!
 *  @method readValue:
 *
 *  @param serviceUUID Service UUID to read from (e.g. 0x2400)
 *  @param characteristicUUID Characteristic UUID to read from (e.g. 0x2401)
 *  @param p CBPeripheral to read from
 *
 *  @discussion Main routine for read value request. It converts integers into
 *  CBUUID's used by CoreBluetooth. It then searches through the peripherals services to find a
 *  suitable service, it then checks that there is a suitable characteristic on this service.
 *  If this is found, the read value is started. When value is read the didUpdateValueForCharacteristic
 *  routine is called.
 *
 *  @see didUpdateValueForCharacteristic
 */

- (NSInteger)readValue:(int)serviceUUID characteristicUUID:(int)characteristicUUID p:(CBPeripheral *)p {
    UInt16 s = [self swap:serviceUUID];
    UInt16 c = [self swap:characteristicUUID];
    NSData *sd = [[NSData alloc] initWithBytes:(char *)&s length:2];
    NSData *cd = [[NSData alloc] initWithBytes:(char *)&c length:2];
    CBUUID *su = [CBUUID UUIDWithData:sd];
    CBUUID *cu = [CBUUID UUIDWithData:cd];
    CBService *service = [self findServiceFromUUID:su p:p];
    if (!service) {
        printf("Could not find service with UUID %s on peripheral with UUID %s\r\n",[self CBUUIDToString:su],[self UUIDToString:p.UUID]);
        return 1;
    }
    CBCharacteristic *characteristic = [self findCharacteristicFromUUID:cu service:service];
    if (!characteristic) {
        printf("Could not find characteristic with UUID %s on service with UUID %s on peripheral with UUID %s\r\n",[self CBUUIDToString:cu],[self CBUUIDToString:su], [self UUIDToString:p.UUID]);
        return 2;
    }
    
    //    CBMutableCharacteristic *rCharacteristic = [[CBMutableCharacteristic alloc] initWithType:characteristic.UUID properties:CBCharacteristicPropertyRead|CBCharacteristicPropertyNotifyEncryptionRequired value:characteristic.value permissions:CBAttributePermissionsReadEncryptionRequired];
    
    //    [p readValueForCharacteristic:rCharacteristic];
    
    [p readValueForCharacteristic:characteristic];
    return 0;
}

/*!
 *  @method soundBuzzer:
 *
 *  @param buzVal The data to write
 *  @param p CBPeripheral to write to
 *
 *  @discussion Sound the buzzer on a TI keyfob. This method writes a value to the proximity alert service
 *
 */
- (void)soundBuzzer:(Byte)buzVal peripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s", __func__);
    NSData *d = [[NSData alloc] initWithBytes:&buzVal length:TI_KEYFOB_PROXIMITY_ALERT_WRITE_LEN];
    [self writeValue:TI_KEYFOB_PROXIMITY_ALERT_UUID characteristicUUID:TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID p:peripheral data:d Response:NO];
    [self performSelector:@selector(stopBuzzer) withObject:nil afterDelay:10];
}

- (void)soundBuzzer:(NSString *)uuid {
    NSLog(@"%s: uuid = %@", __func__, uuid);
    _communicateState = CommunicateStateBuzzer;
    CBPeripheral *peripheral = _euexPhobosBLEManager.knownPeripheralsDict[uuid];
    _buzzerUUID = uuid;
    NSArray *sUUID = @[[CBUUID UUIDWithString:@"1802"]];
    [peripheral discoverServices:sUUID];
}

- (void)soundBuzzer2 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _euexPhobosBLEManager.knownPeripheralsDict[_buzzerUUID];
    [peripheral discoverCharacteristics:nil forService:_buzzerService];
}

- (void)soundBuzzer3 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _euexPhobosBLEManager.knownPeripheralsDict[_buzzerUUID];
    [self soundBuzzer:0x02 peripheral:peripheral];
}

- (void)stopBuzzer {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _euexPhobosBLEManager.knownPeripheralsDict[_buzzerUUID];
    Byte buzVal = 0x00;
    NSData *data = [[NSData alloc] initWithBytes:&buzVal length:TI_KEYFOB_PROXIMITY_ALERT_WRITE_LEN];
    [self writeValue:TI_KEYFOB_PROXIMITY_ALERT_UUID characteristicUUID:TI_KEYFOB_PROXIMITY_ALERT_PROPERTY_UUID p:peripheral data:data Response:NO];
}

//-------------------------------------------------------------------------------------------------------------------
- (void)setLinkLoss:(Byte)buzVal peripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s", __func__);
    NSData *d = [[NSData alloc] initWithBytes:&buzVal length:TI_KEYFOB_LINK_LOSS_WRITE_LEN];
    [self writeValue:TI_KEYFOB_LINK_LOSS_SERVICE_UUID characteristicUUID:TI_KEYFOB_LINK_LOSS_PROPERTY_UUID p:peripheral data:d Response:YES];
}

/** 0:都提醒
 * 1:仅电话提醒
 * 2:仅Phobos提醒
 */
- (void)setLinkLoss:(NSString *)uuid value:(Byte)buzVal {
    NSLog(@"%s: uuid = %@, buzVal = %d", __func__, uuid, buzVal);
    _communicateState = CommunicateStateLinkLoss;
    CBPeripheral *peripheral = _euexPhobosBLEManager.knownPeripheralsDict[uuid];
    _linkLossUUID = uuid;
    NSData *d = [[NSData alloc] initWithBytes:&buzVal length:TI_KEYFOB_LINK_LOSS_WRITE_LEN];
    NSInteger ret = [self writeValue:TI_KEYFOB_LINK_LOSS_SERVICE_UUID characteristicUUID:TI_KEYFOB_LINK_LOSS_PROPERTY_UUID p:peripheral data:d Response:YES];
    NSLog(@"ret = %d", ret);
    if (ret == 0) {
        return;
    }
    linkLossValue = buzVal;
    NSArray *sUUID = @[[CBUUID UUIDWithString:@"1803"]];
    [peripheral discoverServices:sUUID];
}

- (void)setLinkLoss2 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _euexPhobosBLEManager.knownPeripheralsDict[_linkLossUUID];
    [peripheral discoverCharacteristics:nil forService:_linkLossService];
}

- (void)setLinkLoss3 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _euexPhobosBLEManager.knownPeripheralsDict[_linkLossUUID];
    [self setLinkLoss:linkLossValue peripheral:peripheral];
}

//-------------------------------------------------------------------------------------------------------------------
/*!
 *  @method readBattery:
 *
 *  @param p CBPeripheral to read from
 *
 *  @discussion Start a battery level read cycle from the battery level service
 *
 */
- (void)readBatteryPeripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s", __func__);
    [self readValue:TI_KEYFOB_BATT_SERVICE_UUID characteristicUUID:TI_KEYFOB_LEVEL_SERVICE_UUID p:peripheral];
}

- (void)readBattery:(NSString*)uuid {
    NSLog(@"%s", __func__);
    _communicateState = CommunicateStateBattery;
    CBPeripheral *peripheral = _euexPhobosBLEManager.knownPeripheralsDict[uuid];
    _batteryUUID = uuid;
    _retValue = [self readValue:TI_KEYFOB_BATT_SERVICE_UUID characteristicUUID:TI_KEYFOB_LEVEL_SERVICE_UUID p:peripheral];
    NSLog(@"_retValue = %d", _retValue);
    if (_retValue == 0) {
        return;
    }
    
    NSArray *sUUID = @[[CBUUID UUIDWithString:@"180f"]];
    [peripheral discoverServices:sUUID];
}

- (void)readBattery2 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _euexPhobosBLEManager.knownPeripheralsDict[_batteryUUID];
    [peripheral discoverCharacteristics:nil forService:_batteryService];
}

- (void)readBattery3 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _euexPhobosBLEManager.knownPeripheralsDict[_batteryUUID];
    [self readBatteryPeripheral:peripheral];
}

#if defined(GET_TEMPERATURE_USE_BATTERY_SERVICE)
/*!
 *  @method readTemperaturePeripheral:
 *
 *  @param p CBPeripheral to read from
 *
 *  @discussion Start a battery level read cycle from the battery level service
 *
 */
- (void)readTemperaturePeripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s", __func__);
    [self readValue:TI_KEYFOB_TEMPERATURE_SERVICE_UUID characteristicUUID:TI_KEYFOB_TEMPERATURE_CHARACTERISTIC p:peripheral];
}

- (void)readTemperature:(NSString*)uuid {
    NSLog(@"%s: uuid = %@", __func__, uuid);
    _communicateState = CommunicateStateTemperature;
    CBPeripheral *peripheral = _allConnectPeripheral[uuid];
    _temperatureUUID = uuid;
    _retValue = [self readValue:TI_KEYFOB_TEMPERATURE_SERVICE_UUID characteristicUUID:TI_KEYFOB_TEMPERATURE_CHARACTERISTIC p:peripheral];
    NSLog(@"_retValue = %d", _retValue);
    if (_retValue == 0) {
        return;
    } else { // Just for test.
        //        [self performSelector:@selector(testTemperature) withObject:nil afterDelay:1];
        //        return;
    }
    
    NSArray *sUUID = @[[CBUUID UUIDWithString:@"ffb0"]];
    [peripheral discoverServices:sUUID];
}

- (void)readTemperature2 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _allConnectPeripheral[_temperatureUUID];
    [peripheral discoverCharacteristics:nil forService:_temperatureService];
}

- (void)readTemperature3 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _allConnectPeripheral[_temperatureUUID];
    [self readTemperaturePeripheral:peripheral];
}
#else
/*!
 *  @method readTemperaturePeripheral:
 *
 *  @param p CBPeripheral to read from
 *
 *  @discussion Start a battery level read cycle from the battery level service
 *
 */
- (void)readTemperaturePeripheral:(CBPeripheral *)peripheral {
    NSLog(@"%s", __func__);
    [self readValue:TI_KEYFOB_TEMPERATURE_SERVICE_UUID characteristicUUID:TI_KEYFOB_TEMPERATURE_CHARACTERISTIC p:peripheral];
}

- (void)readTemperature:(NSString*)uuid {
    NSLog(@"%s: uuid = %@", __func__, uuid);
    _communicateState = CommunicateStateTemperature;
    CBPeripheral *peripheral = _euexPhobosBLEManager.knownPeripheralsDict[uuid];
    _temperatureUUID = uuid;
    _retValue = [self readValue:TI_KEYFOB_TEMPERATURE_SERVICE_UUID characteristicUUID:TI_KEYFOB_TEMPERATURE_CHARACTERISTIC p:peripheral];
    NSLog(@"_retValue = %d", _retValue);
    if (_retValue == 0) {
        return;
    } else { // Just for test.
        [self performSelector:@selector(testTemperature) withObject:nil afterDelay:1];
        return;
    }
    
    NSArray *sUUID = @[[CBUUID UUIDWithString:@"ffb0"]];
    [peripheral discoverServices:sUUID];
}

- (void)readTemperature2 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _euexPhobosBLEManager.knownPeripheralsDict[_temperatureUUID];
    [peripheral discoverCharacteristics:nil forService:_temperatureService];
}

- (void)readTemperature3 {
    NSLog(@"%s", __func__);
    CBPeripheral *peripheral = _euexPhobosBLEManager.knownPeripheralsDict[_temperatureUUID];
    [self readTemperaturePeripheral:peripheral];
}

- (void)testTemperature {
    NSLog(@"%s", __func__);
    for (id targetDelegate in _delegate) {
        if ([targetDelegate respondsToSelector:@selector(didUpdateTemperature:)]) {
            [targetDelegate didUpdateTemperature:23.9];
        }
    }
}
#endif
@end
