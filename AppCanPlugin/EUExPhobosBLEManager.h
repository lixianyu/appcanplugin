//
//  EUExPhobosFinderBLEManager.h
//  AppCanPlugin
//
//  Created by Li Xianyu on 14-6-18.
//  Copyright (c) 2014年 zywx. All rights reserved.
//

#import "EUExBase.h"
#import "PhobosAdderBLEManager.h"
#import "PhobosFinderBLEManager.h"
#import <CoreBluetooth/CoreBluetooth.h>

typedef NS_ENUM(NSInteger, BLEState){
    BLEStateAdd = 0, //添加新的PHOBOS
    BLEStatePairing,
    BLEStateFind
};

@interface EUExPhobosBLEManager : EUExBase {
    CBCentralManager *bleManager;
    PhobosAdderBLEManager *adderManager;
    PhobosFinderBLEManager *finderManager;
}


@property (strong, nonatomic) CBCentralManager *bleManager;
@property (strong, nonatomic) CBPeripheral *p;
@property (strong, nonatomic) NSMutableDictionary *everConnectedUUID; //所有曾经已连接过的
@property (strong, nonatomic) NSArray *knownPeripherals;
@property (strong, nonatomic) NSMutableDictionary *knownPeripheralsDict;

@property (strong, nonatomic) PhobosAdderBLEManager *adderManager;
@property (strong, nonatomic) PhobosFinderBLEManager *finderManager;
@property (assign, nonatomic) BLEState bleState;
@end
